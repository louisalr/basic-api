from flask import Flask
import os
import threading
import math
import time

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'

@app.route('/exit')
def exit_app():
    os._exit(0)

@app.route('/cpu')
def cpu_intensive():
    def cpu_load():
        start_time = time.time()
        while time.time() - start_time < 120:  # Run for 120 (2m) seconds
            math.factorial(50000)    
    thread = threading.Thread(target=cpu_load)
    thread.start()
    return 'CPU Load Started for 2 minute', 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)