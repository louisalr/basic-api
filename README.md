Commande pour run l'api 


Télécharger minikube 

```bash
docker build -t flask-hello-world .
docker run -p 5000:5000 flask-hello-world
```

Push l'image sur Docker
```
docker build -t your_dockerhub_username/your_image_name .
docker login
docker push your_dockerhub_username/your_image_name
```


Activer l'addon ingress & metrics server

```bash
minikube addons enable ingress
minikube addons enable metrics-server
```